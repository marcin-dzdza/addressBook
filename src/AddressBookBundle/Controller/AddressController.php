<?php

namespace AddressBookBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AddressBookBundle\Entity\Address;
use Symfony\Component\HttpFoundation\Request;

class AddressController extends Controller
{
    /**
     * @Route("/{id}/addAddress", name="addAddressGet")
     * @Method({"GET"})
     */
    public function addAddressGetAction($id)
    {
        $form = $this->getAddAddressForm();

        return $this->render('AddressBookBundle:Address:add_address.html.twig', array(
            'form' => $form->createView()
        ));
    }
    
    /**
     * @Route("/{id}/addAddress", name="addAddressPost")
     * @Method({"POST"})
     */
    public function addAddressPostAction(Request $request, $id)
    {
        $form = $this->getAddAddressForm();
        $form->handleRequest($request);
        
        if (false === $form->isSubmitted()) {
            return $this->redirectToRoute('addAddressGet');
        }
        
        $address = $form->getData();
        
        $repository = $this->getDoctrine()->getRepository('AddressBookBundle:Person');
        $person = $repository->find($id);
        $address->setPerson($person);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($address);
        $em->flush();
        return $this->redirectToRoute('singlePerson', ['id' => $person->getId()]);
    }
    
    private function getAddAddressForm()
    {
        $address = new Address();
        $form = $this->createFormBuilder($address)
            ->add('city', 'text')
            ->add('street', 'text')
            ->add('house', 'text')
            ->add('unit', 'text')
            ->add('save', 'submit', array('label' => 'Submit'))
            ->getForm();
        return $form;
    }
    
    /**
     * @Route("/{id}/deleteAddress", name="deleteAddress")
     * @Method({"GET"})
     */
    public function deleteAddressAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('AddressBookBundle:Address');
        $address = $repository->find($id);
        
        if (!isset($address)) {
            return $this->redirectToRoute('allPersons');
        }
        
        $personId = $address->getPerson()->getId();
        
        $em = $this->getDoctrine()->getManager();
        $em->remove($address);
        $em->flush();
        
        return $this->redirectToRoute('singlePerson', ['id' => $personId]);
    }
}
