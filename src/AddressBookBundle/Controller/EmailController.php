<?php

namespace AddressBookBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AddressBookBundle\Entity\Email;
use Symfony\Component\HttpFoundation\Request;

class EmailController extends Controller
{
    /**
     * @Route("/{id}/addEmail", name="addEmailGet")
     * @Method({"GET"})
     */
    public function addEmailGetAction($id)
    {
        $form = $this->getAddEmailForm();

        return $this->render('AddressBookBundle:Email:add_email.html.twig', array(
            'form' => $form->createView()
        ));
    }
    
    /**
     * @Route("/{id}/addEmail", name="addEmailPost")
     * @Method({"POST"})
     */
    public function addEmailPostAction(Request $request, $id)
    {
        $form = $this->getAddEmailForm();
        $form->handleRequest($request);
        
        if (false === $form->isSubmitted()) {
            return $this->redirectToRoute('addEmailGet');
        }
        
        $email = $form->getData();
        
        $repository = $this->getDoctrine()->getRepository('AddressBookBundle:Person');
        $person = $repository->find($id);
        $email->setPerson($person);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($email);
        $em->flush();
        return $this->redirectToRoute('singlePerson', ['id' => $person->getId()]);
    }
    
    private function getAddEmailForm()
    {
        $email = new Email();
        $form = $this->createFormBuilder($email)
            ->add('value', 'text')
            ->add('type', 'text')
            ->add('save', 'submit', array('label' => 'Submit'))
            ->getForm();
        return $form;
    }
    
    /**
     * @Route("/{id}/deleteEmail", name="deleteEmail")
     * @Method({"GET"})
     */
    public function deleteEmailAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('AddressBookBundle:Email');
        $email = $repository->find($id);
        
        if (!isset($email)) {
            return $this->redirectToRoute('allPersons');
        }
        
        $personId = $email->getPerson()->getId();
        
        $em = $this->getDoctrine()->getManager();
        $em->remove($email);
        $em->flush();
        
        return $this->redirectToRoute('singlePerson', ['id' => $personId]);
    }
}