<?php

namespace AddressBookBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AddressBookBundle\Entity\Person;
use Symfony\Component\HttpFoundation\Request;

class PersonController extends Controller
{
    /**
     * @Route("/new", name="newGet")
     * @Method({"GET"})
     */
    public function newGetAction()
    {
        $form = $this->getAddPersonForm();

        return $this->render('AddressBookBundle:Person:new.html.twig', array(
            'form' => $form->createView()
        ));
    }
    
    /**
     * @Route("/new", name="newPost")
     * @Method({"POST"})
     */
    public function newPostAction(Request $request)
    {
        $form = $this->getAddPersonForm();
        $form->handleRequest($request);
        
        if (false === $form->isSubmitted()) {
            return $this->redirectToRoute('newGet');
        }
        
        $person = $form->getData();
        $em = $this->getDoctrine()->getManager();
        $em->persist($person);
        $em->flush();
        return $this->redirectToRoute('singlePerson', ['id' => $person->getId()]);
    }

    /**
     * @Route("/{id}/modify", name="modifyGet")
     * @Method({"GET"})
     */
    public function modifyGetAction($id)
    {
        $form = $this->getAddPersonForm();

        return $this->render('AddressBookBundle:Person:modify.html.twig', array(
            'form' => $form->createView(),
            'personId' => $id
        ));
    }
    
    /**
     * @Route("/{id}/modify", name="modifyPost")
     * @Method({"POST"})
     */
    public function modifyPostAction(Request $request, $id)
    {
        $form = $this->getAddPersonForm();
        $form->handleRequest($request);
        
        if (false === $form->isSubmitted()) {
            return $this->redirectToRoute('modifyGet');
        }
        
        $repository = $this->getDoctrine()->getRepository('AddressBookBundle:Person');
        $person = $repository->find($id);
        $person->setFirstName($form["firstName"]->getData());
        $person->setLastName($form["lastName"]->getData());
        $person->setDescription($form["description"]->getData());
        
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        
        return $this->redirectToRoute('singlePerson', ['id' => $person->getId()]);
    }
    
    private function getAddPersonForm()
    {
        $person = new Person();
        $form = $this->createFormBuilder($person)
            ->add('firstName', 'text')
            ->add('lastName', 'text')
            ->add('description', 'text')
            ->add('save', 'submit', array('label' => 'Submit'))
            ->getForm();
        return $form;
    }

    /**
     * @Route("/{id}/delete", name="delete")
     * @Method({"GET"})
     */
    public function deleteAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('AddressBookBundle:Person');
        $person = $repository->find($id);
        
        if (!isset($person)) {
            return $this->redirectToRoute('allPersons');
        }
        
        $em = $this->getDoctrine()->getManager();
        $em->remove($person);
        $em->flush();
        
        return $this->redirectToRoute('allPersons');
    }

    /**
     * @Route("/{id}", name="singlePerson")
     * @Method({"GET"})
     */
    public function singlePersonAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('AddressBookBundle:Person');
        $person = $repository->find($id);
        
        return $this->render('AddressBookBundle:Person:single_person.html.twig', array(
            'person' => $person
        ));
    }
    
    /**
     * @Route("/", name="allPersons")
     * @Method({"GET"})
     */
    public function allPersonsAction()
    {
        $repository = $this->getDoctrine()->getRepository('AddressBookBundle:Person');
        $persons = $repository->findAll();
        return $this->render('AddressBookBundle:Person:all_persons.html.twig', array(
            'persons' => $persons
        ));
    }

}
