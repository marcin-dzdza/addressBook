<?php

namespace AddressBookBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AddressBookBundle\Entity\Phone;
use Symfony\Component\HttpFoundation\Request;

class PhoneController extends Controller
{
    /**
     * @Route("/{id}/addPhone", name="addPhoneGet")
     * @Method({"GET"})
     */
    public function addPhoneGetAction($id)
    {
        $form = $this->getAddPhoneForm();

        return $this->render('AddressBookBundle:Phone:add_phone.html.twig', array(
            'form' => $form->createView()
        ));
    }
    
    /**
     * @Route("/{id}/addPhone", name="addPhonePost")
     * @Method({"POST"})
     */
    public function addPhonePostAction(Request $request, $id)
    {
        $form = $this->getAddPhoneForm();
        $form->handleRequest($request);
        
        if (false === $form->isSubmitted()) {
            return $this->redirectToRoute('addPhoneGet');
        }
        
        $phone = $form->getData();
        
        $repository = $this->getDoctrine()->getRepository('AddressBookBundle:Person');
        $person = $repository->find($id);
        $phone->setPerson($person);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($phone);
        $em->flush();
        return $this->redirectToRoute('singlePerson', ['id' => $person->getId()]);
    }
    
    private function getAddPhoneForm()
    {
        $phone = new Phone();
        $form = $this->createFormBuilder($phone)
            ->add('number', 'text')
            ->add('type', 'text')
            ->add('save', 'submit', array('label' => 'Submit'))
            ->getForm();
        return $form;
    }
    
    /**
     * @Route("/{id}/deletePhone", name="deletePhone")
     * @Method({"GET"})
     */
    public function deletePhoneAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('AddressBookBundle:Phone');
        $phone = $repository->find($id);
        
        if (!isset($phone)) {
            return $this->redirectToRoute('allPersons');
        }
        
        $personId = $phone->getPerson()->getId();
        
        $em = $this->getDoctrine()->getManager();
        $em->remove($phone);
        $em->flush();
        
        return $this->redirectToRoute('singlePerson', ['id' => $personId]);
    }
}